package stream

import (
	"bytes"
	"github.com/pkg/errors"
	"io"
)

type Streamer interface {
	Read(b []byte) (int, error)
}

type ResettableStreamer interface {
	Streamer
	Reset()
}

type bufStreamer struct {
	buffer *bytes.Buffer
}

func (s *bufStreamer) Read(b []byte) (int, error) {
	return s.buffer.Read(b)
}

func (s *bufStreamer) Reset() {
	s.buffer.Reset()
}

func FromBuffer(buf *bytes.Buffer) ResettableStreamer {
	return &bufStreamer{
		buffer: buf,
	}
}

type readerStreamer struct {
	reader io.Reader
}

func (r *readerStreamer) Read(b []byte) (int, error) {
	return io.ReadFull(r.reader, b)
	// return r.reader.Read(b)
}

func FromReader(read io.Reader) Streamer {
	return &readerStreamer{
		reader: read,
	}
}

func Take(s Streamer, n int) (Streamer, error) {
	buf := make([]byte, n)
	r, err := s.Read(buf)
	if err != nil {
		return nil, err
	} else if r != n {
		return nil, errors.New("couldn't read enough")
	}
	return &bufStreamer{
		bytes.NewBuffer(buf),
	}, nil
}
